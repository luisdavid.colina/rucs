const { Pool } = require("pg");

const pool = new Pool({
  user: "diamond",
  host: "127.0.0.1",
  database: "sunat",
  password: "lksdfgj53fd",
  port: 5432,
});

const getUsers = async (req, res) => {
  try {
    let response = await pool.query(
      'SELECT * FROM "padron" LIMIT 100'
    );
    res.status(200).json(response.rows);
  } catch (error) {
    res.status(200).json(error);
  }
};

const getUserById = async (req, res) => {
  const id = parseInt(req.params.id);
  const query = 'SELECT * FROM "padron" WHERE "ruc" = $1';
  const response = await pool.query(query, [id]);
  res.json(response.rows[0]);
};

module.exports = {
  getUsers,
  getUserById,
};
